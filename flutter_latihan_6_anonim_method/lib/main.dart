import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String message = "Ini adalah Text";

  // void tombolDitekan() {
  //   setState(() {
  //     message = "Tombol sudah ditekan";
  //   });
  // }

  // sudah diganti anonim methode

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihgan Anonymous Method"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(message),
              // ignore: deprecated_member_use
              RaisedButton(
                  child: Text("Tekan Saya"),
                  onPressed: () {
                    setState(() {
                      message = "Tombol sudah ditekan";
                    });
                  })
              // () {...code...} anonim method mengganti methode tombolDitekan
            ],
          ),
        ),
      ),
    );
  }
}
