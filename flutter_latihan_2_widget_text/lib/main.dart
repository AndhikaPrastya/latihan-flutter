import 'package:flutter/material.dart';

// Fungsi yang pertama dijalankan
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // scaffold widget dasar
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Widget Text"),
        ),
        body: Center(
            child: Container(
                color: Colors.lightBlue,
                width: 150,
                height: 100,
                child: Text(
                  "Hello World Panjang Coba Biar Panjang Banget",
                  // maxLines: 2,
                  // overflow: TextOverflow.clip,
                  // softWrap: false,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.white,
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.w700,
                      fontSize: 20),
                ))),
      ),
    );
  }
}
