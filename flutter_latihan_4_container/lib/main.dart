import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Container"),
        ),
        body: Container(
          color: Colors.cyan,
          margin: EdgeInsets.all(10),
          // ###margin semua sisi
          // margin: EdgeInsets.fromLTRB(10, 15, 20, 25),
          // ###margin per sisi
          // padding: EdgeInsets.all(10),
          // ###padding semua sisi
          padding: EdgeInsets.only(bottom: 10, top: 10, right: 20, left: 20),
          // ###padding sisi tertentu
          child: Container(
            // margin: EdgeInsets.all(10),
            // color: Colors.green,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                // ###border radius pada sudut
                gradient: LinearGradient(
                    begin: Alignment.topLeft, end: Alignment.bottomRight,
                    // ###arah warna gradient
                    colors: <Color>[Colors.amber, Colors.indigo])),
            // ####menambahkan warna gradasi pada boxDecoration
          ),
        ),
      ),
    );
  }
}
