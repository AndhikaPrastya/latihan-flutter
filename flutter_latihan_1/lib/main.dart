import 'package:flutter/material.dart';

// Fungsi yang pertama dijalankan
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // scaffold widget dasar
      home: Scaffold(
        appBar: AppBar(
          title: Text("Aplikasi Hello World"),
        ),
        body: Center(child: Text("Hello World")),
      ),
    );
  }
}
