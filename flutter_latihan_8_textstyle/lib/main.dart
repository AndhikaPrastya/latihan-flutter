import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan Text Style"),
        ),
        body: Center(
          child: Text(
            "Ini adalah text",
            style: TextStyle(
                fontFamily: "Amadeus",
                fontSize: 30,
                decoration: TextDecoration.overline,
                decorationColor: Colors.amber,
                decorationStyle: TextDecorationStyle.wavy,
                decorationThickness: 5),
          ),
        ),
      ),
    );
  }
}
